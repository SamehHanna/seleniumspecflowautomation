﻿using System;
using TechTalk.SpecFlow;
using SeleniumWrapper;
using wrapper = SeleniumWrapper.SeleniumWrapper;
using Xunit;

namespace SeleniumSpecflowAutomation
{
    [Binding]
    public class LoginSteps
    {
        
        private LoginPage loginPage;
        private DashBoardPage dashBoardPage;

        [Given(@"I am on the login screen")]
        public void GivenIAmOnTheLoginScreen()
        {
            //wrapper.CreateChromeWrapper();
            //wrapper.Maximize(SeleniumWrapper.SeleniumWrapper.SeleniumWebDriver());
            //wrapper.NavigateTo("https://devtesting.ebenefitsnetwork.com/eBNPartnerPortal/Account/Login");
            loginPage = new LoginPage();
            LoginPage.NaviagteTo("https://devtesting.ebenefitsnetwork.com/eBNPartnerPortal/Account/Login");

        }
        
        [Given(@"I enter Username of (.*)")]
        public void GivenIEnterUsernameOf(string username)
        {
            //wrapper.SendTextSafe(IdentifierType.ID, "UserName", username);
            loginPage.UserName = username;
        }
        
        [Given(@"I enter password of (.*)")]
        public void GivenIEnterPasswordOf(string password)
        {
            //wrapper.SendTextSafe(IdentifierType.ID, "Password", password);
            loginPage.Password = password;
        }
        
        [When(@"I submit the login")]
        public void WhenISubmitTheLogin()
        {
            //wrapper.ClickSafe(IdentifierType.Class, "submitbtn");
            dashBoardPage=loginPage.LogingIn();
        }
        
        [Then(@"I should see the partner dashboard")]
        public void ThenIShouldSeeThePartnerDashboard()
        {
            Assert.True(dashBoardPage.LoggedIn);
        }

        [AfterScenario]
        public void DisposeWebDriver()
        {
            //wrapper.DisposeWrapper();
        }
    }
}
