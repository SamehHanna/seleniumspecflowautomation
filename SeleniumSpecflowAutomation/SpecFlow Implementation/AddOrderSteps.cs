﻿using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace SeleniumSpecflowAutomation
{
    [Binding]
    public class AddOrderSteps
    {
        private LoginPage loginPage;
        private DashBoardPage dashBoardPage;
        private AddEDIOrderPage addEDIOrderPage;
        private static Random random = new Random();

        [Given(@"I have Logged in using the username (.*) and the password (.*) and using the URL (.*)")]
        public void GivenIHaveLoggedInUsingTheUsernameNadapartnerAndThePassword(string username, string password, string URL)
        {
            loginPage = new LoginPage();
            LoginPage.NaviagteTo(URL);
            loginPage.UserName = username;
            loginPage.Password = password;
            dashBoardPage = loginPage.LogingIn();
        }

        [Given(@"I closed the Pop up")]
        public void GivenClosedThePopUp() => dashBoardPage.ClosePopUp();

        [Given(@"I choose to add EDI Order")]
        public void GivenIChooseToAddEDIOrder() => addEDIOrderPage= dashBoardPage.AddEDIOrder();
        
         
        [Given(@"I have entered in the CustomerName (.*)")]
        public void GivenIHaveEnteredInTheCustomerNameRandomString(string customerName)
            => addEDIOrderPage.customerName = customerName.Contains("Random")?  RandomString(15): customerName;


         
        [Given(@"I have entered in the CustomerId (.*)")]
        public void GivenIHaveEnteredInTheCustomerIdRandomString(string customerId) 
            => addEDIOrderPage.customerId = customerId.Contains("Random")? RandomString(15): customerId;
        
         
        [Given(@"I have entered in the CustomerFTax (.*)")]
        public void GivenIHaveEnteredInTheCustomerFTaxRandomFedex(string customerFTax) 
            => addEDIOrderPage.customerFTax = customerFTax.Contains("Random")? RandomFedTax(): customerFTax;


        [Given(@"I have entered in the CustomerContactName (.*)")]
        public void GivenIHaveEnteredInTheCustomerContactNameCustomerContactName(string customerContactName) => addEDIOrderPage.customerCName = customerContactName;


        [Given(@"I have entered in the CustomerContactPhone (.*)")]
        public void GivenIHaveEnteredInTheCustomerContactPhone(string customerContactPhonep0) => addEDIOrderPage.customerCPhone = customerContactPhonep0;


        [Given(@"I have entered in the CustomerContactEmail (.*)")]
        public void GivenIHaveEnteredInTheCustomerContactEmailAaaBbb_Com(string customerContactEmail) => addEDIOrderPage.customerCEmail = customerContactEmail;


        [Given(@"I have entered in the CustomerStAddress (.*)")]
        public void GivenIHaveEnteredInTheCustomerStAddressCustomerAddress(string customerStAddress) => addEDIOrderPage.customerStAddress = customerStAddress;


        [Given(@"I have entered in the CustomerCity (.*)")]
        public void GivenIHaveEnteredInTheCustomerCityCustomerCity(string customerCity) => addEDIOrderPage.customerCity = customerCity;


        [Given(@"I have entered in the CustomerState (.*)")]
        public void GivenIHaveEnteredInTheCustomerState(string customerState) => addEDIOrderPage.customerState = customerState;


        [Given(@"I have entered in the CustomerZip (.*)")]
        public void GivenIHaveEnteredInTheCustomerZip(string customerZip) => addEDIOrderPage.customerZip = customerZip;


        [Given(@"I have chosen Multiple Companies as (.*)")]
        public void GivenIHaveChosenMultipleCompaniesAsYes(string multiComp) => addEDIOrderPage.multiComp = multiComp.Contains("yes")? "MultiComp" : "singleComp";


        [Given(@"I entered the number of companies as (.*)")]
        public void GivenIEnteredTheNumberOfCompaniesAs(int compNumb=1) => addEDIOrderPage.compNumb = compNumb;
        

        [Given(@"I have entered in the MappingInfo (.*)")]
        public void GivenIHaveEnteredInTheMappingInfoPartner(string mappingInfo) => addEDIOrderPage.mappingInfo = mappingInfo;

        [Given(@"I chose ContactName (.*) and Contact Email (.*) and ContactPhone (.*) and ContactTitle (.*)")]
        public void GivenIChoseContactNameSomeNameAndContactEmailBbbCcc_ComAndContactPhoneAndContactTitleSomeTitle
            (string oContactName,string oContactEmail, string oContactPhone, string oContactTitle)
        {
            addEDIOrderPage.otherContact = new string[] { oContactName, oContactEmail, oContactPhone, oContactTitle };
        }


        [Given(@"I have entered in the ContactTitle (.*)")]
        public void GivenIHaveEnteredInTheContactTitleContactTitle(string contactTitle) => addEDIOrderPage.contactTitle = contactTitle;


        [Given(@"I have entered in the ContactName (.*)")]
        public void GivenIHaveEnteredInTheContactNameContactName(string contactName) => addEDIOrderPage.contactName = contactName;


        [Given(@"I have entered in the ContactPhone (.*)")]
        public void GivenIHaveEnteredInTheContactPhone(string contactPhone) => addEDIOrderPage.contactPhone = contactPhone;


        [Given(@"I have entered in the ContactEmail (.*)")]
        public void GivenIHaveEnteredInTheContactEmailAaaBbb_Com(string contactEmail) => addEDIOrderPage.contactEmail = contactEmail;


        [Given(@"I have entered in the CompanyName (.*)")]
        public void GivenIHaveEnteredInTheCompanyNameCompany(string companyName) => addEDIOrderPage.companyName = companyName;


        [Given(@"I have entered in the CompanyCity (.*)")]
        public void GivenIHaveEnteredInTheCompanyCityCity(string companyCity) => addEDIOrderPage.companyCity = companyCity;


        [Given(@"I have entered in the CompanyState (.*)")]
        public void GivenIHaveEnteredInTheCompanyState(string companyState) => addEDIOrderPage.companyState = companyState;

         
        [Given(@"I have entered in the CompanyFedTax (.*)")]
        public void GivenIHaveEnteredInTheCompanyFedTaxRandomFedex(string companyFedTax)
                => addEDIOrderPage.companyFedTax = companyFedTax.Contains("Random")? RandomFedTax() : companyFedTax;

        [Given(@"I have entered in the CompanyAddress (.*)")]
        public void GivenIHaveEnteredInTheCompanyAddressCompanyAddress(string CompanyAddress) => addEDIOrderPage.companyAddress = CompanyAddress;


        [Given(@"I have entered in the CarrierCount (.*)")]
        public void GivenIHaveEnteredInTheCarrierCount(int carrierCount) => addEDIOrderPage.carrierCount = carrierCount;


        [Given(@"I have entered in the EmployeeCount (.*)")]
        public void GivenIHaveEnteredInTheEmployeeCount(int employeeCount) => addEDIOrderPage.employeeCount = employeeCount;


        [Given(@"I have entered in the PlanStartDate (.*)")]
        public void GivenIHaveEnteredInThePlanStartDate(string planStartDate) => addEDIOrderPage.planStart = planStartDate;


        [Given(@"I have entered in the EnrollmentMonth (.*)")]
        public void GivenIHaveEnteredInTheEnrollmentMonthJanuary(string enrollmentMonth) => addEDIOrderPage.enrollmentMonth = enrollmentMonth;


        [Given(@"I have entered in the CustomerDataETA (.*)")]
        public void GivenIHaveEnteredInTheCustomerDataETA(string customerDataETA) => addEDIOrderPage.customerDataETA = customerDataETA;


        [Given(@"I have entered in the SpecialInst (.*)")]
        public void GivenIHaveEnteredInTheSpecialInstNothing(string specialInst) => addEDIOrderPage.specialInst = specialInst;


        [Given(@"I have entered in the Carrier (.*)")]
        public void GivenIHaveEnteredInTheCarierCarrier(string carrierName) => addEDIOrderPage.carrierName = carrierName;

         
        [Given(@"I have entered in the CarrierFedEx (.*)")]
        public void GivenIHaveEnteredInTheCarrierFedExRandomFedex(string carrierFedEx) 
                => addEDIOrderPage.carrierFedTax = carrierFedEx.Contains("Random")? RandomFedTax() : carrierFedEx;

        [Given(@"I have entered in the GroupNumber (.*)")]
        public void GivenIHaveEnteredInTheGroupNumberGroupNumber(string groupNumber) => addEDIOrderPage.groupNumber = groupNumber;


        [Given(@"I have entered in the CarrierCName (.*)")]
        public void GivenIHaveEnteredInTheCarrierCNameCarrierContactName(string carrierCName) => addEDIOrderPage.carrierCName = carrierCName;


        [Given(@"I have entered in the CarrierCPhone (.*)")]
        public void GivenIHaveEnteredInTheCarrierCPhone(string carrierCPhone) => addEDIOrderPage.carrierCPhone = carrierCPhone;


        [Given(@"I have entered in the CarrierCEmail (.*)")]
        public void GivenIHaveEnteredInTheCarrierCEmailAaaBbb_Com(string carrierCEmail) => addEDIOrderPage.carrierCEmail = carrierCEmail;


        [Given(@"I have entered in the CarrierCAName (.*)")]
        public void GivenIHaveEnteredInTheCarrierCANameCarrierContactName(string carrierCAName) => addEDIOrderPage.carrierCAName = carrierCAName;


        [Given(@"I have entered in the CarrierCAPhone (.*)")]
        public void GivenIHaveEnteredInTheCarrierCAPhone(string carrierCAPhone) => addEDIOrderPage.carrierCAPhone = carrierCAPhone;


        [Given(@"I have entered in the CarrierCAEmail (.*)")]
        public void GivenIHaveEnteredInTheCarrierCAEmailAaaBbb_Com(string carrierCAEmail) => addEDIOrderPage.carrierCAEmail = carrierCAEmail;


        [Given(@"I have entered in the Cobra (.*)")]
        public void GivenIHaveEnteredInTheCobraFalse(string cobra) => addEDIOrderPage.cobra = cobra;

        [Given(@"I have entered in select all companies (.*)")]
        public void GivenIHaveEnteredInSelectAllCompaniesNo(string AllCompanies) => addEDIOrderPage.allCompanies = AllCompanies;


        [Given(@"I choose the Company Number (.*)")]
        public void GivenIRemovedTheCompanyNumber(int companySelected) => addEDIOrderPage.companySelected = companySelected;
        


        [Given(@"I have entered in the DentalPlanChecked (.*)")]
        public void GivenIHaveEnteredInTheDentalPlanCheckedTrue(int dentalPlanChecked) => addEDIOrderPage.dentalChecked = dentalPlanChecked != 0;


        [Given(@"I have entered in the MedicalPlanChecked (.*)")]
        public void GivenIHaveEnteredInTheMedicalPlanCheckedTrue(int medicalPlanChecked) => addEDIOrderPage.medicalChecked = medicalPlanChecked != 0;


        [Given(@"I have entered in the TestReq (.*)")]
        public void GivenIHaveEnteredInTheTestReqYes(string testReq) => addEDIOrderPage.testReq = testReq;


        [Given(@"I have entered in the Upload (.*)")]
        public void GivenIHaveEnteredInTheUploadEAll_Xlsx(string upload) => addEDIOrderPage.upload = upload;


        [When(@"I press save")]
        public void WhenIPressSave() => addEDIOrderPage.Save();

        [Then(@"I should see the save confirmation")]
        public void ThenIShouldSeeTheConfirmation() => addEDIOrderPage.Saved();

        [When(@"I press submit")]
        public void WhenIPressSubmit() => addEDIOrderPage.Submit();

        [Then(@"I should see the submit confirmation")]
        public void ThenIShouldSeeTheSubmitConfirmation() => addEDIOrderPage.Submitted();

       



        private string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        private string RandomFedTax()
        {
            Random generator = new Random();
            String r = generator.Next(0, 999999999).ToString("D9");
            return r;
        }

    }
}
