﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumWrapper;
using wrapper = SeleniumWrapper.SeleniumWrapper;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace SeleniumSpecflowAutomation
{
    

    public class DashBoardPage
    {
        public bool LoggedIn { get { return wrapper.GetElementSafe(IdentifierType.ID, "autoComplete").Displayed; } }

        public DashBoardPage() {

            //PageFactory.InitElements(wrapper.SeleniumWebDriver(), this);
        }

        public void ClosePopUp()
        {
            System.Threading.Thread.Sleep(5000);
            wrapper.ClickSafe(IdentifierType.CssSelector, "#wm-shoutout-88711 > div.wm-content > div.buttons-wrapper > span");
        }

        public AddEDIOrderPage AddEDIOrder()
        {
            wrapper.ClickSafe(IdentifierType.Class, "greenboxlink");
            wrapper.ClickSafe(IdentifierType.ID, "AddEDI");
            return new AddEDIOrderPage();
        }

       
    }
}
