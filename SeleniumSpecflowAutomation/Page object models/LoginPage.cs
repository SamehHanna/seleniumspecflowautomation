﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumWrapper;
using wrapper = SeleniumWrapper.SeleniumWrapper;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumSpecflowAutomation
{
    public class LoginPage
    {
        
        

        public string UserName { set { wrapper.SendTextSafe( IdentifierType.ID, "UserName", value); } }
        public string Password { set { wrapper.SendTextSafe( IdentifierType.ID, "Password", value); } }

        public LoginPage()
        {
            wrapper.CreateFireFoxWrapper(60);
            wrapper.Maximize(wrapper.SeleniumWebDriver());
                      
        }
        public static void NaviagteTo(string URL) => wrapper.NavigateTo(URL);
        public DashBoardPage LogingIn()
        {
            wrapper.ClickSafe( IdentifierType.Class, "submitbtn");
            return new DashBoardPage();
        }
    }
}
