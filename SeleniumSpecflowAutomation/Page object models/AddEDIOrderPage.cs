﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumWrapper;
using Xunit;
using wrapper = SeleniumWrapper.SeleniumWrapper;

namespace SeleniumSpecflowAutomation
{
    public class AddEDIOrderPage
    {
        private bool firstCompany,firstCarrier;
         
        //Partner Contact Information
        public string contactName { set { wrapper.SendTextSafe(IdentifierType.ID, "partnerContactName", value); } }
        public string contactPhone { set { wrapper.SendTextSafe(IdentifierType.ID, "partnerContactPhone", value); } }
        public string contactEmail { set { wrapper.SendTextSafe(IdentifierType.ID, "partnerContactEmail", value); } }
        public string contactTitle { set { wrapper.SendTextSafe(IdentifierType.ID, "partnerContactTitle", value); } }

        //Customer Information
        public string customerName { set { wrapper.SendTextSafe(IdentifierType.ID, "CustomerName", value); } }
        public string customerId { set { wrapper.SendTextSafe(IdentifierType.ID, "CustomerCode", value); } }
        public string customerFTax { set { wrapper.SendTextSafe(IdentifierType.ID, "FedralTaxID", value); } }
        public string customerStAddress { set { wrapper.SendTextSafe(IdentifierType.ID, "CustomerAdd", value); } }
        public string customerCity { set { wrapper.SendTextSafe(IdentifierType.ID, "CustomerCity", value); } }
        public string customerState { set { wrapper.DropDownSelectByValueSafe(IdentifierType.ID, "StateID", value); } }
        public string customerZip { set { wrapper.SendTextSafe(IdentifierType.ID, "PrimaryCode", value); } }
        public string customerCName { set { wrapper.SendTextSafe(IdentifierType.ID, "ContactName", value); } }
        public string customerCPhone { set { wrapper.SendTextSafe(IdentifierType.ID, "ContactNumber", value); } }
        public string customerCEmail { set { wrapper.SendTextSafe(IdentifierType.ID, "ContactEmail", value); } }
        string mutliCompI = "";
        public string multiComp { set { wrapper.ClickSafe(IdentifierType.ID, value); mutliCompI = value; } }
        int compNumbI = 1;
        public int compNumb { set {
                if (mutliCompI.Contains("MultiComp"))
                { wrapper.SendTextSafe(IdentifierType.ID, "Compnumb", value.ToString());
                    compNumbI = value; }
            } }


        //Maping Information
        bool otherMapping = false;
        public string mappingInfo { set { wrapper.DropDownSelectByValueSafe(IdentifierType.ID, "ContactList", value);
                otherMapping = value.Contains("Other");
            } }
        public string[] otherContact
        {
            set
            {
                if (otherMapping)
                {
                    wrapper.SendTextSafe(IdentifierType.ID, "Mname", value[0]);
                    wrapper.SendTextSafe(IdentifierType.ID, "Memail", value[1]);
                    wrapper.SendTextSafe(IdentifierType.ID, "Mphone", value[2]);
                    wrapper.SendTextSafe(IdentifierType.ID, "Mtitle", value[3]);
                }
            }
        }

        // New Company
        public string companyName { set {SetMultipleText(IdentifierType.ID, "CompName_", value,"hc", ref firstCompany, compNumbI);} }
        public string companyCity { set { SetMultipleText(IdentifierType.ID, "CompCity_", value, "hc", ref firstCompany, compNumbI); } }
        public string companyState { set { SetMultipleCombo(IdentifierType.ID, "CompStateID_",value, "hc", ref firstCompany, compNumbI); } }
        public string companyFedTax { set { SetMultipleTextNoChange(IdentifierType.ID, "CompFedralTaxID_", value, "hc", ref firstCompany, compNumbI); } }

       
        public string companyAddress { set { SetMultipleText(IdentifierType.ID, "CompAddress_", value, "hc", ref firstCompany, compNumbI); } }

        //Customer Benefits Information
        int carrierCountI = 1;
        public int carrierCount { set { wrapper.SendTextSafe(IdentifierType.ID, "ConnectionsNumber", value.ToString()); carrierCountI = value; } }
        public int employeeCount { set { wrapper.SendTextSafe(IdentifierType.ID, "EmployeesNumber", value.ToString()); } }
        public string planStart { set { wrapper.SendTextSafe(IdentifierType.ID, "PlanYearStartDate", value); } }
        public string enrollmentMonth { set { wrapper.DropDownSelectByValueSafe(IdentifierType.ID, "EnrollmentMonth", value); } }
        public string customerDataETA { set { wrapper.SendTextSafe(IdentifierType.ID, "CustomerDataETA", value); } }
        public string specialInst { set { wrapper.SendTextSafe(IdentifierType.ID, "OrderComments", value); } }

        // New carrier
        public string carrierName { set { SetMultipleText(IdentifierType.ID, "CarrierName_", value,"h",ref firstCarrier, carrierCountI); } }
        public string carrierFedTax { set { SetMultipleTextNoChange(IdentifierType.ID, "FedralTaxID_", value, "h", ref firstCarrier, carrierCountI); } }
        public string groupNumber { set { SetMultipleText(IdentifierType.ID, "GroupNumber_", value, "h", ref firstCarrier, carrierCountI); } }
        public string carrierCName { set { SetMultipleText(IdentifierType.ID, "ContactName_", value, "h", ref firstCarrier, carrierCountI); } }
        public string carrierCPhone { set { SetMultipleText(IdentifierType.ID, "ContactPhone_", value, "h", ref firstCarrier, carrierCountI); } }
        public string carrierCEmail { set { SetMultipleTextNoChange(IdentifierType.ID, "ContactEmail_", value, "h", ref firstCarrier, carrierCountI); } }
        public string carrierCAName { set { SetMultipleText(IdentifierType.ID, "SecContactName_", value, "h", ref firstCarrier, carrierCountI); } }
        public string carrierCAPhone { set { SetMultipleText(IdentifierType.ID, "SecContactPhone_", value, "h", ref firstCarrier, carrierCountI); } }
        public string carrierCAEmail { set { SetMultipleTextNoChange(IdentifierType.ID, "SecContactEmail_", value, "h", ref firstCarrier, carrierCountI); } }
        public string cobra { set { SetMultipleCombo(IdentifierType.ID, "CobraMembers_", value, "h", ref firstCarrier, carrierCountI); } }
        public string testReq { set { SetMultipleText(IdentifierType.ID, "carriers[0].customfiedls[0].value", value, "h", ref firstCarrier, carrierCountI); } }
        public bool dentalChecked { set { SetMultipleCheckBox(IdentifierType.ID, "Carrier_0_PlanChecked_1", "h", ref firstCarrier, carrierCountI); } }
        public bool medicalChecked { set { SetMultipleCheckBox(IdentifierType.ID, "Carrier_0_PlanChecked_6", "h", ref firstCarrier, carrierCountI); } }
        string allCompaniesI = "";
        public string allCompanies { set {
                if (value.Contains("No"))
                    wrapper.ClickSafe(IdentifierType.ID, "selcomp_0");
                else
                    wrapper.ClickSafe(IdentifierType.ID, "all_0");
                allCompaniesI = value;
            } }
        public int companySelected { set {
                if (allCompaniesI.Contains("No"))
                {
                    wrapper.ClickSafe(IdentifierType.ID, "SelectCompLink_0");
                    string check = value == 0 ? "Chkall" : "company_" + (value - 1);
                    //if (value==0) wrapper.CheckACheckBoxSafe(IdentifierType.ID, "Chkall");
                    //else wrapper.CheckACheckBoxSafe(IdentifierType.ID, "company_" + (value-1));
                    wrapper.CheckACheckBoxSafe(IdentifierType.ID, check);
                    wrapper.ClickSafe(IdentifierType.CssSelector, "div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(1)");
                }

            } }

        private IWebElement saveButton=wrapper.GetElementSafe(IdentifierType.ID, "UASave");
        private IWebElement submitButton = wrapper.GetElementSafe(IdentifierType.ID, "UASub");
        

        // Attach Order Files
        public string upload { set { wrapper.SendTextSafe(IdentifierType.Class, "attachinput", value); } }

        

        public AddEDIOrderPage()
        {
            //PageFactory.InitElements(wrapper.SeleniumWebDriver(), this);
            firstCompany = true;
            firstCarrier = true;
            wrapper.ClickSafe(IdentifierType.ID, "opennotifi");
        }

        public void SetMultipleText(IdentifierType iD, string idIncr, string value, string header, ref bool first, int count)
        {
            if (first || count == 1)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + 0);
                first = false;
            }
            for (int i = 0; i < count; i++)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + i);
                System.Threading.Thread.Sleep(1500);
                wrapper.SendTextSafe(iD, idIncr + i, value + i );
            }
        }

        private void SetMultipleTextNoChange(IdentifierType iD, string idIncr, string value, string header, ref bool first, int count)
        {
            if (first || count == 1)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + 0);
                first = false;
            }
            for (int i = 0; i < count; i++)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + i);
                System.Threading.Thread.Sleep(1500);
                wrapper.SendTextSafe(iD, idIncr + i, value);
            }
        }

        private void SetMultipleCombo(IdentifierType iD, string idIncr, string value, string header, ref bool first, int count)
        {
            if (first || count ==1)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + 0);
                first = false;
            }
            for (int i = 0; i < count; i++)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + i);
                System.Threading.Thread.Sleep(2000);
                wrapper.DropDownSelectByValueSafe(iD, idIncr + i, value);
            }
        }

        private void SetMultipleCheckBox(IdentifierType iD, string idValue, string header, ref bool first, int count)
        {
            if (first || count == 1)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + 0);
                first = false;
            }
            for (int i = 0; i < compNumbI; i++)
            {
                wrapper.ClickSafe(IdentifierType.ID, header + i);
                System.Threading.Thread.Sleep(1500);
                wrapper.ClickSafe(iD, idValue.Replace("_0_", "_" + (i).ToString()+"_"));
            }

        }

        


        public void Save() => wrapper.ClickSafe(saveButton, IdentifierType.ID, "UASave");
      

        public void Saved() => Assert.True(wrapper.ClickSafe(IdentifierType.ID, "success-id"));

        public void Submit()
        { 
            wrapper.ClickSafe(submitButton, IdentifierType.ID, "UASub");
            wrapper.ClickSafe(IdentifierType.ID, "confirm_id");
        }
        public void Submitted() => Assert.True(wrapper.ClickSafe(IdentifierType.ID, "submit_id"));
        
    }
}
