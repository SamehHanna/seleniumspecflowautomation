﻿Feature: AddOrder
	In order to Login and fill new EDI/OE/DF/Ticket order
	As a partner
	I want to check if order was added correctly


Scenario: Save new EDI order
	Given I have Logged in using the username nadapartner and the password 123123 and using the URL https://devtesting.ebenefitsnetwork.com/eBNPartnerPortal
		And I closed the Pop up
		And I choose to add EDI Order
		#Partner Contact Start
		And I have entered in the ContactTitle Contact Title
 		And I have entered in the ContactName Contact Name
		And I have entered in the ContactPhone 1234567890
		And I have entered in the ContactEmail aaa2@bbb.com
		#Partner Contact End
		#Customer Start
		And I have entered in the CustomerName RandomString
		And I have entered in the CustomerId RandomString
		And I have entered in the CustomerFTax RandomFedex
		And I have entered in the CustomerContactName Customer Contact Name
		And I have entered in the CustomerContactPhone 1234567890
		And I have entered in the CustomerContactEmail aaa2@bbb.com
		And I have entered in the CustomerStAddress Customer Address
		And I have entered in the CustomerCity Customer City
		And I have entered in the CustomerState 1
		And I have entered in the CustomerZip 12345
		And I have chosen Multiple Companies as yes
		And I entered the number of companies as 3
 #Customer End
 # Select the contact used for Customer Customer
		And I have entered in the MappingInfo Other
		And I chose ContactName Some Name and Contact Email bbb@ccc.com and ContactPhone 0123456789 and ContactTitle Some Title
  #Company Start
		And I have entered in the CompanyName Some Company
 		And I have entered in the CompanyCity Some City
		And I have entered in the CompanyState 1
		And I have entered in the CompanyFedTax RandomFedex
		And I have entered in the CompanyAddress Company Address
 #Company End
		And I have entered in the CarrierCount 3
 # Customer Benefit Info Start
		And I have entered in the EmployeeCount 100
		And I have entered in the PlanStartDate 07/06/2018
		And I have entered in the EnrollmentMonth January
		And I have entered in the CustomerDataETA 07/06/2018
		And I have entered in the SpecialInst Nothing
 # Customer Benefit info End
 # Carrier Start
		And I have entered in the Cobra False
		And I have entered in the Carrier Some Carrier 
		And I have entered in the CarrierFedEx RandomFedex
		And I have entered in the GroupNumber Group Number
		And I have entered in the CarrierCName Carrier Contact Name
		And I have entered in the CarrierCPhone 1234567890
		And I have entered in the CarrierCEmail aaa@bbb.com
		And I have entered in the CarrierCAName Carrier Contact Name
		And I have entered in the CarrierCAPhone 1234567890
		And I have entered in the CarrierCAEmail aaa@bbb.com
		
		# Enter Yes or No
		And I have entered in select all companies No
		# If you eneter No and Company Number 0 it will check select all
		And I choose the Company Number 0
		And I have entered in the DentalPlanChecked 1
		And I have entered in the MedicalPlanChecked 1
 # Carrier End
		And I have entered in the TestReq yes
		And I have entered in the Upload E:\all.xlsx
 # Attach File
	When I press save
	Then I should see the save confirmation

	Scenario: Submit new EDI order
	Given I have Logged in using the username nadapartner and the password 123123 and using the URL https://devtesting.ebenefitsnetwork.com/eBNPartnerPortal
		And I closed the Pop up
		And I choose to add EDI Order
		#Partner Contact Start
		And I have entered in the ContactTitle Contact Title
 		And I have entered in the ContactName Contact Name
		And I have entered in the ContactPhone 1234567890
		And I have entered in the ContactEmail aaa2@bbb.com
		#Partner Contact End
		#Customer Start
		And I have entered in the CustomerName RandomString
		And I have entered in the CustomerId RandomString
		And I have entered in the CustomerFTax RandomFedex
		And I have entered in the CustomerContactName Customer Contact Name
		And I have entered in the CustomerContactPhone 1234567890
		And I have entered in the CustomerContactEmail aaa2@bbb.com
		And I have entered in the CustomerStAddress Customer Address
		And I have entered in the CustomerCity Customer City
		And I have entered in the CustomerState 1
		And I have entered in the CustomerZip 12345
		And I have chosen Multiple Companies as yes
		And I entered the number of companies as 2
 #Customer End
 # Select the contact used for Customer Customer
		And I have entered in the MappingInfo Partner
  #Company Start
		And I have entered in the CompanyName Some Company
 		And I have entered in the CompanyCity Some City
		And I have entered in the CompanyState 1
		And I have entered in the CompanyFedTax RandomFedex
		And I have entered in the CompanyAddress Company Address
 #Company End
		And I have entered in the CarrierCount 1
 # Customer Benefit Info Start
		And I have entered in the EmployeeCount 100
		And I have entered in the PlanStartDate 07/06/2018
		And I have entered in the EnrollmentMonth January
		And I have entered in the CustomerDataETA 07/06/2018
		And I have entered in the SpecialInst Nothing
 # Customer Benefit info End
 # Carrier Start
		And I have entered in the Carrier Some Carrier 
		And I have entered in the CarrierFedEx RandomFedex
		And I have entered in the GroupNumber Group Number
		And I have entered in the CarrierCName Carrier Contact Name
		And I have entered in the CarrierCPhone 1234567890
		And I have entered in the CarrierCEmail aaa@bbb.com
		And I have entered in the CarrierCAName Carrier Contact Name
		And I have entered in the CarrierCAPhone 1234567890
		And I have entered in the CarrierCAEmail aaa@bbb.com
		And I have entered in the Cobra False
		And I have entered in the DentalPlanChecked 1
		And I have entered in the MedicalPlanChecked 1
 # Carrier End
		And I have entered in the TestReq yes
		And I have entered in the Upload E:\all.xlsx
 # Attach File
	When I press submit
	Then I should see the submit confirmation
