﻿Feature: Login
	In order to Login 
	As a partner
	I want to enter login credentials

	Scenario: Login with nadapartner
		Given I am on the login screen
			And  I enter Username of nadapartner
			And I enter password of 123123
		When I submit the login
		Then I should see the partner dashboard 

	Scenario: Login with samehgoco
		Given I am on the login screen
		And I enter Username of samehgoco
		And I enter password of 123456
		When I submit the login
		Then I should see the partner dashboard